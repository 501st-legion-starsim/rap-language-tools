import { setServers } from 'dns';
import { type } from 'os';
import { Connection, Diagnostic, DiagnosticSeverity } from 'vscode-languageserver';
import { TextDocument } from 'vscode-languageserver-textdocument';

async function validateCoupledPairs(doc: TextDocument): Promise<any> {
	const diagnostics: Diagnostic[] = [];

	const text = doc.getText();

	const stack = [];
	const pairs = {
		"{": -1,
		"(": -2,
		"[": -3,

		"}": 1,
		")": 2,
		"]": 3,
	};

	let m: RegExpExecArray | null;
	const pattern = /{|}|\[|\]|\(|\)|("[^"]*")|('[^']*')|'|"/g;

	while ((m = pattern.exec(text))) {
		const val = pairs[m[0]];

		if (typeof val !== "undefined") {
			if (val < 0 && validBracketPair(stack)) {
				// Starting bracket.
				stack.push(val);
			} else if (val > 0 && val < 10 && validBracketPair(stack)) {
				// Ending bracket.
				const len = stack.length - 1;
				if (len < 0) {
					diagnostics.push({
						severity: DiagnosticSeverity.Error,
						range: {
							start: doc.positionAt(m.index),
							end: doc.positionAt(m.index + m[0].length)
						},
						message: `Unexpected ${getItemFromValue(val, pairs)} - missing opening pair.`,
						source: 'raP'
					});
				} else if (stack[len] + val == 0) {
					stack.pop();
				} else {
					diagnostics.push({
						severity: DiagnosticSeverity.Error,
						range: {
							start: doc.positionAt(m.index),
							end: doc.positionAt(m.index + m[0].length)
						},
						message: `Expected ${inverseBracketPair(stack[len], pairs)} - found ${getItemFromValue(val, pairs)}`,
						source: 'raP'
					});
				}
			}
		} else {
			// Quote Marks.

			if (m[0] == '"' || m[0] == "'") {
				diagnostics.push({
					severity: DiagnosticSeverity.Error,
					range: {
						start: doc.positionAt(m.index),
						end: doc.positionAt(m.index + m[0].length)
					},
					message: `No closing quote found. Expected ${m[0]}`,
					source: 'raP'
				});
			} else if (m[0].indexOf("\n") !== -1) {
				diagnostics.push({
					severity: DiagnosticSeverity.Error,
					range: {
						start: doc.positionAt(m.index),
						end: doc.positionAt(m.index + m[0].length)
					},
					message: `Quotes can not span multiple lines.`,
					source: 'raP'
				});
			} else {
				continue;
			}
		}
	}

	if (stack.length > 0) {
		const len = text.length - 1;
		stack.forEach((e) => {
			diagnostics.push({
				severity: DiagnosticSeverity.Error,
				range: {
					start: doc.positionAt(len - 1),
					end: doc.positionAt(len)
				},
				message: `Expected closing ${inverseBracketPair(e, pairs)}`,
				source: 'raP'
			});
		});
	}

	return diagnostics;
}

function validBracketPair(stack) {
	const len = stack.length - 1;

	if (len < 0) return true;

	return stack[len] < 10;
}

function getItemFromValue(val, pairs) {
	for (const [key, value] of Object.entries(pairs)) {
		if (value == val) return key;
	}
	return "";
}

function inverseBracketPair(val, pairs) {
	for (const [key, value] of Object.entries(pairs)) {
		if (value == -val) return key;
	}
	return "";
}

export { validateCoupledPairs };