import { Connection, Diagnostic, DiagnosticSeverity } from 'vscode-languageserver';
import { TextDocument } from 'vscode-languageserver-textdocument';

async function validateSemicolons(doc: TextDocument): Promise<any> {
	const results = [
		await validateClassStatements(doc),
		await validateIncludeStatements(doc),
		await validateArrayStatements(doc),
		await validateIndependentOpenBrackets(doc),
		await validateLineEndings(doc)
	];
	
	//const diagnostics: Diagnostic[] = [];

	//return diagnostics;

	const dat = [];
	results.forEach((e: []) => {
		e.forEach((x: Diagnostic[]) => {
			dat.push(x);
		});
	});

	return dat;
}

async function validateLineEndings(doc: TextDocument): Promise<any> {
	const diagnostics: Diagnostic[] = [];

	const text = doc.getText();
	
	let m: RegExpExecArray | null;
	let pattern = /}([^\n\w])*(;[^\n])*/gm;

	while ((m = pattern.exec(text))) {
		const count = m[0].match(/;/g)?.length ?? 0;
		if (count > 1) {
			diagnostics.push({
				severity: DiagnosticSeverity.Error,
				range: {
					start: doc.positionAt(m.index + m[0].indexOf(';') + 1),
					end: doc.positionAt(m.index + m[0].lastIndexOf(';'))
				},
				message: `Found too many semicolons (;).`,
				source: 'raP'
			});
		} else if (count < 1) {
			diagnostics.push({
				severity: DiagnosticSeverity.Error,
				range: {
					start: doc.positionAt(m.index + m[0].length),
					end: doc.positionAt(m.index + m[0].length)
				},
				message: `Missing semicolon (;).`,
				source: 'raP'
			});
		}
	}

	pattern = /^.*=.*/gm;

	while ((m = pattern.exec(text))) {
		let count = m[0].match(/\[\]\s*=\s*{?[^}]*?$/gm)?.length ?? 0;		
		if (count > 0) continue;

		count = m[0].match(/;/g)?.length ?? 0;
		if (count > 1) {
			diagnostics.push({
				severity: DiagnosticSeverity.Error,
				range: {
					start: doc.positionAt(m.index + m[0].indexOf(';') + 1),
					end: doc.positionAt(m.index + m[0].lastIndexOf(';'))
				},
				message: `Found too many semicolons (;).`,
				source: 'raP'
			});
		} else if (count < 1) {
			diagnostics.push({
				severity: DiagnosticSeverity.Error,
				range: {
					start: doc.positionAt(m.index + m[0].length),
					end: doc.positionAt(m.index + m[0].length)
				},
				message: `Missing semicolon (;).`,
				source: 'raP'
			});
		}
	}

	return diagnostics;
}

async function validateIndependentOpenBrackets(doc: TextDocument): Promise<any> {
	const diagnostics: Diagnostic[] = [];

	const text = doc.getText();

	let m: RegExpExecArray | null;
	const pattern = /{[^\w}]*?;/gm;

	while ((m = pattern.exec(text))) {
		diagnostics.push({
			severity: DiagnosticSeverity.Error,
			range: {
				start: doc.positionAt(m.index + m[0].indexOf(';')),
				end: doc.positionAt(m.index + m[0].indexOf(';'))
			},
			message: `Found ; expected }.`,
			source: 'raP'
		});
	}

	return diagnostics;
}

async function validateArrayStatements(doc: TextDocument): Promise<any> {
	const diagnostics: Diagnostic[] = [];

	const text = doc.getText();

	let m: RegExpExecArray | null;
	const pattern = /\w*\[\][\s\S]*?}/gm;

	while ((m = pattern.exec(text))) {
		if (m[0].indexOf(';') !== -1) {
			diagnostics.push({
				severity: DiagnosticSeverity.Error,
				range: {
					start: doc.positionAt(m.index + m[0].indexOf(';')),
					end: doc.positionAt(m.index + m[0].indexOf(';'))
				},
				message: `Found a semicolon inside an array declaration.`,
				source: 'raP'
			});
		}
	}

	return diagnostics;
}

async function validateIncludeStatements(doc: TextDocument): Promise<any> {
	const diagnostics: Diagnostic[] = [];

	const text = doc.getText();

	let m: RegExpExecArray | null;
	const pattern = /^\s*#include[^\n]*$/gm;

	while ((m = pattern.exec(text))) {
		if (m[0].indexOf(';') !== -1) {
			diagnostics.push({
				severity: DiagnosticSeverity.Error,
				range: {
					start: doc.positionAt(m.index + m[0].length),
					end: doc.positionAt(m.index + m[0].length)
				},
				message: `Include declarations can not end in a semicolon.`,
				source: 'raP'
			});
		}
	}

	return diagnostics;
}

async function validateClassStatements(doc: TextDocument): Promise<any> {
	const diagnostics: Diagnostic[] = [];

	const text = doc.getText();

	let m: RegExpExecArray | null;
	const pattern = /^\s*class[^\n]*$/gm;

	while ((m = pattern.exec(text))) {
		if (m[0].indexOf(';') !== -1) {
			if (!m[0].includes("}")) {
			diagnostics.push({
					severity: DiagnosticSeverity.Error,
					range: {
						start: doc.positionAt(m.index + m[0].length),
						end: doc.positionAt(m.index + m[0].length)
					},
					message: `Class declarations can not end in a semicolon.`,
					source: 'raP'
				});
			}
		}
	}

	return diagnostics;
}

export { validateSemicolons };